import os

konec = 0
kdoHraje = 0
def init():
    global konec, kdoHraje

    pole = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]

    while konec == 0:
        herniKolo(pole)

def clearScreen():
    if os.name == "posix":
        os.system("clear")
    if os.name == "nt":
        os.system("cls")


def vypisHru(pole):
    for i in range(3):
        print(f"[{pole[i][0]}] [{pole[i][1]}] [{pole[i][2]}]")

def check(pole):
    # horizontal checking
    # 0 is for when 'o's win and 1 is for when 'x's win
    for i in range(3):
        if pole[i][0] == pole[i][1] == pole[i][2] == 'o':
            return 0
        if pole[i][0] == pole[i][1] == pole[i][2] == 'x':
            return 1
    
    # vertical checking
    for i in range(3):
        if pole[0][i] == pole[1][i] == pole[2][i] == 'o':
            return 0
        if pole[0][i] == pole[1][i] == pole[2][i] == 'x':
            return 1
        
    # diagonal checking
    if pole[0][0] == pole[1][1] == pole[2][2] == 'o':
        return 0
    if pole[0][0] == pole[1][1] == pole[2][2] == 'x':
        return 1
    if pole[0][2] == pole[1][1] == pole[2][0] == 'o':
        return 0
    if pole[0][2] == pole[1][1] == pole[2][0] == 'x':
        return 1

    # draw checking
    # 2 is for when a draw occurs
    pocet = 0
    for i in range(3):
        for j in range(3):
            if pole[i][j] == 'o' or pole[i][j] == 'x':
                pocet += 1
    if pocet == 9:
        return 2

def herniKolo(pole):
    global konec, kdoHraje

    # algorithm steps
    # 
    # 1 clear the screen
    # 2 print the current state of the game
    # 3 allow for one player or the other to insert coords
    # 4 place an 'o' or an 'x' to the coresponding coords
    # 5 check if the game should end (if someone won)
    # 5.1 if yes -> end the game
    # 5.2 if no -> jump to step 1

    # step 1 -> clear the screen
    # 
    # cross-platform (Windows, Unix-like OSs)
    clearScreen()

    # step 2 -> print the current state of the game
    vypisHru(pole)

    # step 3 -> allow for one player or the other to insert coords
    rada, sloupec = map(int, input().split())

    # step 4 -> place an 'o' or an 'x' to the coresponding coords
    # 
    # only if there isn't another symbol present and the coords are valid
    # the 'o' or 'x' is determined by the kdoHraje variable
    # - if it is 0, then an 'o' is placed, if it is 1, an 'x' is placed instead
    # the rows and columns are marked 1, 2 and 3, not 0, 1 and 2 as the indexes
    #   for better user experience
    if pole[rada-1][sloupec-1] == ' ' and 1 <= rada <= 3 and 1 <= sloupec <= 3:
        print(kdoHraje)
        if kdoHraje == 0:
            pole[rada-1][sloupec-1] = 'o'
            kdoHraje = 1
        elif kdoHraje == 1:
            pole[rada-1][sloupec-1] = 'x'
            kdoHraje = 0

    # step 5 -> check if the game should end (if someone won)
    vyherce = check(pole)

    if vyherce == 0:
        # step 5.1 -> if yes -> end the game
        clearScreen()
        print("Vyhralo o!")
        vypisHru(pole)
        konec = 1
    # step 5.2 -> if no -> jump to step 1

    if vyherce == 1:
        clearScreen()
        print("Vyhralo x!")
        vypisHru(pole)
        konec = 1

    if vyherce == 2:
        clearScreen()
        print("Remiza!")
        vypisHru(pole)
        konec = 1

if __name__ == "__main__":
    init()