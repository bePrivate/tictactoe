# TicTacToe

A simple CLI TicTacToe-like game written in Python
Latest version - v2.1.3

## Key features

+ Variable playfield size
+ Ability to insert player names

## Gameplay

Upon startup, you are prompted to enter the playfield size, for
example: you enter `3`, so the playfield area will be 3 rows high 
and 3 columns wide. Then you enter the name of player one and
player two. The first player always begins. 3 by 3 is the smallest
possible playfield area size.

When the game itself starts, you are prompted to enter the coordinates
of the field you want your symbol to go into. For example: if your
symbol is 'o' and you enter `1 1`, then an 'o' will be placed in the
top left corner of the playfield area.  

A game ends, when one player or the other has placed 3 of their symbols
in either a horizontal row, a vertical row, or a diagonal row. There
can also be a draw, that is, when none of the players have managed
to place 3 of their symbols in a row.