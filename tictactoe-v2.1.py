# algorithm steps
# 
# prerequisites
# 01 allow a user to determine the size of the playfield
# 02 allow a user to enter names of two players
# 
# 1 clear the screen
# 2 print the current state of the game
# 3 allow for one player or the other to insert coords
# 4 place an 'o' or an 'x' to the coresponding coords
# 5 check if the game should end (if someone won)
# 5.1 if yes -> end the game
# 5.2 if no -> jump to step 1

import os

konec = 0
kdoHraje = 0
def init():
    global konec, kdoHraje

    # prerequisite 01 -> allow a user to determine the size of the playfield
    size = int(input())

    # prerequisite 02 -> allow a user to enter names of two players
    jmeno1 = input()
    jmeno2 = input()

    pole = []
    temp = []
    for i in range(size):
        for j in range(size):
            temp.append(' ')
        pole.append(temp)
        temp = []

    while konec == 0:
        herniKolo(pole, jmeno1, jmeno2)

def clearScreen():
    if os.name == "posix":
        os.system("clear")
    if os.name == "nt":
        os.system("cls")


def vypisHru(pole, jmeno1, jmeno2):
    print(jmeno1, "(o) vs", jmeno2, "(x)")
    for i in range(len(pole)):
        for j in range(len(pole)):
            print(f"[{pole[i][j]}]", end=" ")
        print(" ")

def check(pole):
    # horizontal checking
    # 0 is for when 'o's win and 1 is for when 'x's win
    o = x = 0
    for i in range(len(pole)):
        for j in range(len(pole)):
            if pole[i][j] == 'o':
                o += 1
            if pole[i][j] == 'x':
                x += 1
    if o >= 3:
        return 0
    elif x >= 3:
        return 1
    
    # vertical checking
    o = x = 0
    for i in range(len(pole)):
        for j in range(len(pole)):
            if pole[j][i] == 'o':
                o += 1
            if pole[j][i] == 'x':
                x += 1
    if o >= 3:
        return 0
    elif x >= 3:
        return 1
            
    # diagonal checking
    for i in range(len(pole)):
        for j in range(len(pole)):
            if pole[i][j] == 'o':
                # only 6 possibilities, so the program will try all of them
                if pole[i-1][j-1] == 'o' and pole[i-2][j-2] == 'o':
                    return 0
                if pole[i-1][j+1] == 'o' and pole[i-2][j+2] == 'o':
                    return 0
                if pole[i+1][j-1] == 'o' and pole[i+2][j-2] == 'o':
                    return 0
                if pole[i+1][j+1] == 'o' and pole[i+2][j+2] == 'o':
                    return 0
                if pole[i-1][j-1] == 'o' and pole[i+1][j+1] == 'o':
                    return 0
                if pole[i-1][j+1] == 'o' and pole[i+1][j-1] == 'o':
                    return 0
                
    for i in range(len(pole)):
        for j in range(len(pole)):
            if pole[i][j] == 'x':
                if pole[i-1][j-1] == 'x' and pole[i-2][j-2] == 'x':
                    return 1
                if pole[i-1][j+1] == 'x' and pole[i-2][j+2] == 'x':
                    return 1
                if pole[i+1][j-1] == 'x' and pole[i+2][j-2] == 'x':
                    return 1
                if pole[i+1][j+1] == 'x' and pole[i+2][j+2] == 'x':
                    return 1
                if pole[i-1][j-1] == 'x' and pole[i+1][j+1] == 'x':
                    return 1
                if pole[i-1][j+1] == 'x' and pole[i+1][j-1] == 'x':
                    return 1

    # draw checking
    # 2 is for when a draw occurs
    pocet = 0
    for i in range(len(pole)):
        for j in range(len(pole)):
            if pole[i][j] == 'o' or pole[i][j] == 'x':
                pocet += 1
    if pocet == len(pole)*len(pole):
        return 2

def herniKolo(pole, jmeno1, jmeno2):
    global konec, kdoHraje

    # step 1 -> clear the screen
    # 
    # cross-platform (Windows, Unix-like OSs)
    clearScreen()

    # step 2 -> print the current state of the game
    vypisHru(pole, jmeno1, jmeno2)

    # step 3 -> allow for one player or the other to insert coords
    rada, sloupec = map(int, input().split())

    # step 4 -> place an 'o' or an 'x' to the coresponding coords
    # 
    # only if there isn't another symbol present and the coords are valid
    # the 'o' or 'x' is determined by the kdoHraje variable
    # - if it is 0, then an 'o' is placed, if it is 1, an 'x' is placed instead
    # the rows and columns are marked 1, 2 and 3, not 0, 1 and 2 as the indexes
    #   for better user experience
    if pole[rada-1][sloupec-1] == ' ' and 1 <= rada <= len(pole)-1 and 1 <= sloupec <= len(pole)-1:
        if kdoHraje == 0:
            pole[rada-1][sloupec-1] = 'o'
            kdoHraje = 1
        elif kdoHraje == 1:
            pole[rada-1][sloupec-1] = 'x'
            kdoHraje = 0

    # step 5 -> check if the game should end (if someone won)
    vyherce = check(pole)

    if vyherce == 0:
        # step 5.1 -> if yes -> end the game
        clearScreen()
        print("Vyhralo o!")
        vypisHru(pole, jmeno1, jmeno2)
        konec = 1
    # step 5.2 -> if no -> jump to step 1

    if vyherce == 1:
        clearScreen()
        print("Vyhralo x!")
        vypisHru(pole, jmeno1, jmeno2)
        konec = 1

    if vyherce == 2:
        clearScreen()
        print("Remiza!")
        vypisHru(pole, jmeno1, jmeno2)
        konec = 1

if __name__ == "__main__":
    init()